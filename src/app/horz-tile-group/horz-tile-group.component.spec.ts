import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HorzTileGroupComponent } from './horz-tile-group.component';

describe('HorzTileGroupComponent', () => {
  let component: HorzTileGroupComponent;
  let fixture: ComponentFixture<HorzTileGroupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HorzTileGroupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HorzTileGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
