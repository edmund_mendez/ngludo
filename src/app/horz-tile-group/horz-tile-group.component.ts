import { Component, OnInit, Input } from '@angular/core';
import {Tile} from '../tile';
@Component({
  selector: 'app-horz-tile-group',
  templateUrl: './horz-tile-group.component.html',
  styleUrls: ['./horz-tile-group.component.css']
})
export class HorzTileGroupComponent implements OnInit {
  @Input()
  tiles: Array<Tile>;
  constructor() { }

  ngOnInit() {
  }

}
