import {Yard} from './yard';

export class Piece {
    private color: string;
    private style = {};
    constructor(private yard: Yard) {
        this.color = this.yard.getColor();
    }
    getYard(): Yard{
        return this.yard;
    }
    getColor(): string {
        return this.color;
    }
}
