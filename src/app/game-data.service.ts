import { Injectable } from '@angular/core';
import {TileGroup} from './tile-group';
import {Player} from './player';
import { Yard } from './yard';
import {Observable} from 'rxjs/Observable';
import {Subject} from 'rxjs/Subject';
import {} from 'rxjs/interval';
import {Tile} from './tile';

@Injectable()
export class GameDataService {
  regions: Array<TileGroup> = [];
  players: Array<Player> = [];
  yards: Array<Yard> = [];
  colors: Array<string> = ['red', 'black', 'blue', 'green'];
  gameState: GameState = GameState.AWAIT_START;

  /* rxjs streams used by the game service to monitor user activity */
  tileClick$: Subject<Tile> = new Subject<Tile>();
  pieceMove$: Subject<Array<Tile>> = new Subject<Array<Tile>>();

  tileClickHistory: Array<Tile> = [];

  constructor() {
    this.setupGame();
  }
  confRegions() {
    this.regions.push(new TileGroup(), new TileGroup(), new TileGroup(), new TileGroup());
    this.regions[0].setNeighbor(this.regions[1]);
    this.regions[1].setNeighbor(this.regions[2]);
    this.regions[2].setNeighbor(this.regions[3]);
    this.regions[3].setNeighbor(this.regions[0]);
  }
  getRegion(regionId: number): TileGroup {
    return this.regions[regionId];
  }
  confPlayers() {
    this.players.push(new Player(), new Player(), new Player(), new Player());
  }
  // for each region, create and configure yard and asign user
  confYards() {
    this.regions.forEach( (region, i) => {
      this.yards.push(new Yard(i, this.colors[i], this.regions));
    });
  }
  getYards() {
    return this.yards;
  }
  setupGame() {
    this.confRegions();
    this.confYards();
    this.confPlayers();
    this.monitorTileClick();
    this.monitorPieceMove();
  }
  doHighlight(row: number, col: number){
    this.regions[0].getTile(row, col).isHighlighted = !this.regions[0].getTile(row,col).isHighlighted;
  }

  onTileClick(tile: Tile) {
    this.tileClick$.next(tile);
  }

 // TODO: REMOVE SECOND OBSERVABLE AND MAKE THE MOVE FUNCTION BE A FUNCTION CALL - MAKES IT EASIER TO PRESERVE GAME STATE IF INVALID DESITATION IS SELECTED **/
  monitorTileClick() {
    this.tileClick$.subscribe((tile) => {
      this.tileClickHistory.push(tile);
      if(this.tileClickHistory.length === 2){ // ***** Necessary hack until I get Observable.bufferCount(2) to work ***********
        this.pieceMove$.next(this.tileClickHistory.slice());  //pass a copy of array to observable, since we are emptying it below
        this.tileClickHistory.length = 0; // empty the array
      }
    });
  }

  monitorPieceMove() {
    this.pieceMove$.subscribe((tiles) => {
      if (tiles[0].getNoPieces() === 0 || tiles[1].getNoPieces() > 0){
        return;
      }
      // move piece
      if(tiles[0].getPieces()[0].getYard().isTileOnYardPath(tiles[1])) {  //check if destination is valid
        tiles[1].addPiece(tiles[0].popPiece());
      }else{
        alert('invalid desitation. Select source and destination again.');
      }

    });
  }
}

export enum GameState {
  AWAIT_START,
  AWAIT_TURN,
  AWAIT_MOVE
}