import { TileGroup } from './tile-group';
import { Tile } from './tile';
import {Piece} from './piece';

export class Yard {
    private tilePath: Array<Tile> = []; // internal linear arrangement of tiles along path from yard to destination
    private pieces: Array<Piece> = [];
    constructor(private regionKey: number, private color: string, private regions: Array<TileGroup>) {
        this.mapPath();
        this.createPieces();
       // this.createYardTiles();
    }

    createPieces() {
        this.pieces.push(new Piece(this), new Piece(this), new Piece(this), new Piece(this));
    }

    getPiece(position: number): Piece {
        return this.pieces[position];
    }

    mapPath() {
        const startGroup: TileGroup = this.regions[this.regionKey];
        let neighborGroup: TileGroup = startGroup.getNeighbor();

        this.tilePath = this.tilePath.concat(startGroup.getRightTiles());   // start of tiles for this yard on way to destination
        //remove first item from array to get where the files actually start
        this.tilePath.splice(0, 1);
        while (neighborGroup !== startGroup) {
            this.tilePath = this.tilePath.concat(neighborGroup.getLTRTiles());  // all the tiles that run 'around' a group
            neighborGroup = neighborGroup.getNeighbor();
        }
        // now that we are where we origianlly started, just add the left and middle tiles that run down to the destination
        this.tilePath = this.tilePath.concat(startGroup.getLeftTiles());
        this.tilePath = this.tilePath.concat(startGroup.getMiddleTiles());
    }

    getColor(): string {
        return this.color;
    }

    highlightPath() {
        this.tilePath.forEach( (tile ) => {tile.isHighlighted = true;
        });
    }
    isTileOnYardPath(tile: Tile): boolean {
        return this.tilePath.indexOf(tile) >= 0 ;
    }
}
