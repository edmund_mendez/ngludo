import { Component, OnInit, Input } from '@angular/core';
import {Tile} from '../tile';
import {GameDataService} from '../game-data.service';
import {Piece} from '../piece';
@Component({
  selector: 'app-tile',
  templateUrl: './tile.component.html',
  styleUrls: ['./tile.component.css']
})
export class TileComponent implements OnInit {
  @Input () tile: Tile;
  piece: Piece;
  tileStyle = {};
  constructor(private gameDataService: GameDataService) { }

  onClick(){
    // this.tile.isHighlighted = !this.tile.isHighlighted;
    this.gameDataService.onTileClick(this.tile);
  }

  ngOnInit() {
    this.piece = this.tile.getNoPieces() > 0 ? this.tile.getPieces()[0] : null;
  }

}
