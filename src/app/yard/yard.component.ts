import { Component, OnInit, Input } from '@angular/core';
import {Yard} from '../yard';
import { Tile } from '../tile';
@Component({
  selector: 'app-yard',
  templateUrl: './yard.component.html',
  styleUrls: ['./yard.component.css']
})
export class YardComponent implements OnInit {
  @Input() yard: Yard;
  private yardTiles: Array<Tile> = [];    // tiles that will host pieces
  style = {};
  constructor() { }

  ngOnInit() {
    this.setColor();
    this.createYardTiles();
  }

  createYardTiles() {
    this.yardTiles.push(new Tile(),new Tile(),new Tile(),new Tile());
    this.yardTiles[0].addPiece(this.yard.getPiece(0));
    this.yardTiles[1].addPiece(this.yard.getPiece(1));
    this.yardTiles[2].addPiece(this.yard.getPiece(2));
    this.yardTiles[3].addPiece(this.yard.getPiece(3));
  }

  highlight() {
    //console.log(this.yard.getColor());
    this.yard.highlightPath();
  }
  setColor() {
    this.style = {
      'background-color': this.yard.getColor()
    };
  }
}
