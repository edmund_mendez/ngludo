import { Component, OnInit, Input } from '@angular/core';
import {Tile} from '../tile';

@Component({
  selector: 'app-vert-tile-group',
  templateUrl: './vert-tile-group.component.html',
  styleUrls: ['./vert-tile-group.component.css']
})
export class VertTileGroupComponent implements OnInit {
  @Input()
  tiles: Array<Tile>;
  constructor() { }

  ngOnInit() {
  }

}
