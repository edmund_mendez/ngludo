import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VertTileGroupComponent } from './vert-tile-group.component';

describe('VertTileGroupComponent', () => {
  let component: VertTileGroupComponent;
  let fixture: ComponentFixture<VertTileGroupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VertTileGroupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VertTileGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
