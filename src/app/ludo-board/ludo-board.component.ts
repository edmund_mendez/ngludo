import { Component, OnInit } from '@angular/core';
import {GameDataService} from '../game-data.service';
import {Tile} from '../tile';
import {Yard} from '../yard';

@Component({
  selector: 'app-ludo-board',
  templateUrl: './ludo-board.component.html',
  styleUrls: ['./ludo-board.component.css']
})
export class LudoBoardComponent implements OnInit {
  private tilesTop: Array<Array<Tile>>;
  private tilesRight: Array<Array<Tile>>;
  private tilesBottom: Array<Array<Tile>>;
  private tilesLeft: Array<Array<Tile>>;
  private yards: Array<Yard>;

  constructor(private gameData: GameDataService) { }

  ngOnInit() {
    this.tilesTop = this.gameData.getRegion(0).layoutTop();
    this.tilesRight = this.gameData.getRegion(1).layoutRight();
    this.tilesBottom = this.gameData.getRegion(2).layoutBottom();
    this.tilesLeft = this.gameData.getRegion(3).layoutLeft();
    this.yards = this.gameData.getYards();
  }

}
