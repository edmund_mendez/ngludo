import {Piece} from './piece';
export class Tile {
    isLocked?: boolean;
    isHighlighted = false;
   private pieces: Array<Piece> = [];
    constructor() {
    }
    getPieces(): Array<Piece> {
        return this.pieces;
    }
    getNoPieces(): number {
        return this.pieces.length;
    }
    addPiece(piece) {
        this.pieces.push(piece);
    }

    removePiece(piece): Piece {
        this.pieces.splice(this.pieces.indexOf(piece), 1);
        return piece;
    }

    popPiece(): Piece {
        return this.pieces.pop();
    }
}
