import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { LudoBoardComponent } from './ludo-board/ludo-board.component';
import { GameDataService } from './game-data.service';
import { YardComponent } from './yard/yard.component';
import { VertTileGroupComponent } from './vert-tile-group/vert-tile-group.component';
import { HorzTileGroupComponent } from './horz-tile-group/horz-tile-group.component';
import { DestinationComponent } from './destination/destination.component';
import { TileComponent } from './tile/tile.component';
import { PieceComponent } from './piece/piece.component';

@NgModule({
  declarations: [
    AppComponent,
    LudoBoardComponent,
    YardComponent,
    VertTileGroupComponent,
    HorzTileGroupComponent,
    DestinationComponent,
    TileComponent,
    PieceComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [GameDataService],
  bootstrap: [AppComponent]
})

export class AppModule { }
