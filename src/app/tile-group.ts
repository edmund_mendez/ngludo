import {Tile} from './tile';

export class TileGroup {
    private tileMap: Array<Array<Tile>> = [];
    private groupNeighbor: TileGroup;   //tile group adjacent, going clockwise
    rows = 6;

    constructor() {
        this.createTiles();
    }
    setNeighbor(neighbor: TileGroup) {
        this.groupNeighbor = neighbor;
    }
    getNeighbor(): TileGroup {
        return this.groupNeighbor;
    }

    createTiles() {
        for (let i = 1; i <= this.rows; i++) {
            this.tileMap[i - 1] = [new Tile(), new Tile(), new Tile()];
        }
    }

    getTile(row: number, col: number): Tile {
        return this.tileMap[row][col];
    }

    // get left top and right tiles as an array
    getLTRTiles(): Tile[] {
        let tileArray: Tile[] = [];

        tileArray = tileArray.concat(this.getLeftTiles());
        tileArray.push(this.tileMap[0][1]); // top tile: tile that connects left and right
        tileArray  = tileArray.concat(this.getRightTiles());
        return tileArray;
    }

    getLeftTiles(): Tile[] {
        const tileArray: Tile[] = [];

        for (let i = this.rows; i >= 1; i--) {   // left tiles
            tileArray.push(this.tileMap[i - 1][0]);
        }
        return tileArray;
    }

    getRightTiles(): Tile[] {
        const tileArray: Tile[] = [];
        for (let i = 1; i <= this.rows; i++) {   // right tiles
            tileArray.push(this.tileMap[i - 1][2]);
        }

        return tileArray;
    }

    getMiddleTiles(): Tile[] {
        const tileArray: Tile[] = [];
        for (let i = 1; i <= this.rows; i++) {   // middle tiles
            tileArray.push(this.tileMap[i - 1][1]);
        }

        return tileArray;
    }
    /** these functions are required for layout purposes. Creates easliy navigatable two dimensional array to mimic board layout */
    // this function receives an array representing upright group and can rotate
    layoutRight(): Array<Array<Tile>> {
        // this rotation return convert 6 row, 3 column array to 3 row 6 column
        const tileArray: Array<Array<Tile>> = [];
        tileArray[0] = this.getLeftTiles();
        tileArray[1] = this.getMiddleTiles().reverse();
        tileArray[2] = this.getRightTiles().reverse();
        return tileArray;
    }

    layoutBottom(): Array<Array<Tile>> {
        // flip upright array 180 degrees
        const tileArray: Array<Array<Tile>> = [[], [], [], [], [], []];
        for (let i = 1, j = this.rows; i <= this.rows; i++) {
            tileArray[i - 1][0] = this.tileMap[j - 1][0];
            tileArray[i - 1][1] = this.tileMap[j - 1][1];
            tileArray[i - 1][2] = this.tileMap[j - 1][2];
            tileArray[i - 1].reverse();
            j--;
        }
        return tileArray;
    }

    layoutLeft(): Array<Array<Tile>> {
        // this rotation return convert 6 row, 3 column array to 3 row 6 column
        const tileArray: Array<Array<Tile>> = [];
        tileArray[0] = this.getRightTiles();
        tileArray[1] = this.getMiddleTiles();
        tileArray[2] = this.getLeftTiles().reverse();
        return tileArray;
    }

    layoutTop(): Array<Array<Tile>> {
        return this.tileMap;
    }
}
