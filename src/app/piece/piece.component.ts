import { Component, OnInit, Input, OnChanges } from '@angular/core';
import {Piece} from '../piece';
@Component({
  selector: 'app-piece',
  templateUrl: './piece.component.html',
  styleUrls: ['./piece.component.css']
})
export class PieceComponent implements OnInit {
  @Input() piece: Piece;
  color = 'black';  // default piece color
  pieceStyle = {};
  constructor() { }

  ngOnInit() {
    this.color = this.piece.getColor();
    this.pieceStyle = {
      'color': this.color,
      'font-size': '2vh'
    }
  }
  }
