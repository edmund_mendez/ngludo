import { NgLudoPage } from './app.po';

describe('ng-ludo App', () => {
  let page: NgLudoPage;

  beforeEach(() => {
    page = new NgLudoPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
